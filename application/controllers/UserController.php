<?php

use \Interop\Container\ContainerInterface;
use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;

namespace application\controllers;

class UserController {
    protected $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function get(ServerRequestInterface $req, ResponseInterface $res, $args = []) {
        try {
            $users = $this->userService->getAll();

            if (!is_array($users) || count($users) == 0)
                return $res->withStatus(204)->write('No users encountered.');

            return $res->withJson($users);
        } catch(Exception $ex) {
            return $res->withStatus(400)->write('An error has occurred. ' . $ex);
        }
    }

    public function add($req, $res, $args = []) {
        try {
            // TODO Finish add method
        } catch (Exception $ex) {
            return $res->withStatus(400)->write('An error has occurred. ' . $ex);
        }
    }

    // TODO Add update and delete methods
}
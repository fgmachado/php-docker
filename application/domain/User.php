<?php

namespace application\domain;

class User {
    public $id;
    public $name;
    public $email;
    
    public function __construct($name, $email) {
        $this->name = $name;
        $this->email = $email;
    }
}
<?php

namespace application\services;

class UserService {
    private $nextId = 1;
    public $users;

    public function __construct() {
        $this->mock(new \application\domain\User('Felipe Gomes Machado', 'fgmachado0@gmail.com'));
    }

    public function getAll() {
        return $this->users;
    }

    public function add(User $user) {
        $this->mock($user);
    }

    // TODO Add update and delete methods

    private function mock($user) {
        if ($this->users == null)
            $this->users = array();

        if ($user->id == null)
            $user->id = $this->nextId++;

        array_push($this->users, $user);
    }
}
docker build -t php-docker:dev .
docker stop %2
docker rm %2
docker run -p %1:80 --name %2 php-docker:dev
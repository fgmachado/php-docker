<?php

require_once 'application/vendor/autoload.php';

spl_autoload_register(function ($classname) {
    $classname = str_replace('\\', '/', $classname);

    require_once $classname . '.php';
});

$container = new \Slim\Container;
$container['UserService'] = function ($container) {
    return new UserService();
};

$app = new \Slim\App($container);

$app->get('/', application\controllers\HomeController::class . '::index');
$app->get('/users', application\controllers\UserController::class . '::get');
$app->post('/users', application\controllers\UserController::class . '::add');
$app->put('/users/{id:[0-9]+}', application\controllers\UserController::class . '::update');
$app->delete('/users/{id:[0-9]+}', application\controllers\UserController::class . '::delete');

$app->run();
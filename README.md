# PHP Docker

This project

### Requirements

To run this project, you'll need install Docker for Windows and Composer.

### Run

Clone the repository or download a zip file with the source code an unzip.
Navigate to the source directory and run the next command to install all project dependencies, using composer.

`composer install`

After, run php-docker-run.bat, adding two parameters after the batch file.
The first parameter is the port that will be used to access the application.
Second parameter is the name of container, this will be used to create a container.
Run the next command to start the container with the application:

`php-docker-run.bat 8082 php-docker`
